package com.classscheduler.syllabusparsing;

import com.classscheduler.domain.Assignment;
import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 @author HarshaGoli */
public class DocAnalysis {
	String theFile;
	ArrayList<Assignment> asmt = new ArrayList<Assignment>();
	//ArrayList
	String textOfFile;

	public DocAnalysis (String TheFile) {
		this.theFile = TheFile;
		//    this.asmt = AssignmentArray();
	}

	public boolean evaluateDoc () {
		int response = 0;
		try {
			PDFTextParser pdfTextParserObj = new PDFTextParser();
			this.textOfFile = pdfTextParserObj.pdfToText(theFile);
		} catch (Exception e) {
			response += 1;
		}

		return response == 0;
	}

	public ArrayList<Assignment> parseAssignments () {
		System.out.println("\n\n\n\nBeginning of Parsing");
		BufferedReader bufferedReader = new BufferedReader(new StringReader(textOfFile));
		try {
			String lineWithDate = null;
			while ((lineWithDate = bufferedReader.readLine()) != null) {
				lineWithDate = lineWithDate.trim();
				String lineUnderLineWithDate;
				String line2IsNotADate = null;
				// If the line in question is not empty (i.e. filled with any values), then proceed

				if (Pattern.matches("\\D*(?:(\\d*)\\/(\\d{1,2})\\/(\\d{2,4}))|(?:(\\d*)\\-(\\d{1,2})\\-(\\d{2,4}))|(?:((?:Dec(?:ember)*)|(?:Jan(?:uary)*)|(?:Feb(?:ruary)*)|(?:March)|(?:April)|(?:May)|(?:June)|(?:July)|(?:Aug(?:ust)*)|(?:Sept(?:ember)*)|(?:Sep(?:tember)*)|(?:Nov(?:ember)*))( \\d{1,2}))\\D*", lineWithDate)) {
					//If the line in question has a date of some form, then proceed
					int checkVariable = 0;
					if (lineWithDate.length() != 10 || lineWithDate.length() != 9) {
						checkVariable = 1;
						line2IsNotADate = lineWithDate;

						//This is to see if the line in question has only a date, or has other information on the same line.
						//it does this by checking the number of elements on the line.
						//The normal "09/11/2001 has 10 elements; 9/11/2001 has 9 elements
						//If the line has more or less elements, then checkVariable is 0, activating the while loop below
						//If not, then Line2 is not a date

						//TODO: 11/4 - ERROR IN LOGIC****************
						//this IF statement is using incorrect logic. The logic is "If the line in question is a date, and has 9 or 10 elements
						//THEN do not activate the loop below and define Line2IsNotADate as line(which is a date)
					}
					;
					while (checkVariable == 0) {
						//System.out.println("Stage2");
						lineUnderLineWithDate = bufferedReader.readLine().trim();

						if (Pattern.matches("\\D*(?:(\\d*)\\/(\\d{1,2})\\/(\\d{2,4}))|(?:(\\d*)\\-(\\d{1,2})\\-(\\d{2,4}))|(?:((?:Dec(?:ember)*)|(?:Jan(?:uary)*)|(?:Feb(?:ruary)*)|(?:March)|(?:April)|(?:May)|(?:June)|(?:July)|(?:Aug(?:ust)*)|(?:Sept(?:ember)*)|(?:Sep(?:tember)*)|(?:Nov(?:ember)*))( \\d{1,2}))\\D*", lineUnderLineWithDate)) {
							lineWithDate = lineUnderLineWithDate;
						} else {
							line2IsNotADate = lineUnderLineWithDate;
							break;
						}

						//This while loop, when activated, checks if the line under the line with a date also has a line
						//if the line under LineWithDate contains a date, then the lineUnderLineWithDate becomes the new LineWithDate and the while loop reloops to check the next
						//if not, then line2IsNotADate will equal lineUnderLineWithDate
						//this means the there is a line with a date, and underneath is something that is not a date, more than likely information regarding the date
					}
					scanDocument(line2IsNotADate, lineWithDate);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Now we return!!");
		return asmt;
	}

	public void scanDocument (String line2IsNotADate, String lineWithDate) {
		Assignment x = new Assignment();
		System.out.printf("lineWithDate: %s%nline2IsNotADate: %s%n", lineWithDate, line2IsNotADate);
		if (Pattern.compile(".*(test|homework|quiz|project).*", Pattern.CASE_INSENSITIVE).matcher(line2IsNotADate).matches()) {
			if (Pattern.compile(".*(project).*", Pattern.CASE_INSENSITIVE).matcher(line2IsNotADate).matches()) {
				x.setAssignmentType(Assignment.AssignmentType.Project);
			}
			if (Pattern.compile(".*(test).*", Pattern.CASE_INSENSITIVE).matcher(line2IsNotADate).matches()) {
				x.setAssignmentType(Assignment.AssignmentType.Test);
			}
			if (Pattern.compile(".*(quiz).*", Pattern.CASE_INSENSITIVE).matcher(line2IsNotADate).matches()) {
				x.setAssignmentType(Assignment.AssignmentType.Quiz);
			}
			if (Pattern.compile(".*(homework).*", Pattern.CASE_INSENSITIVE).matcher(line2IsNotADate).matches()) {
				x.setAssignmentType(Assignment.AssignmentType.Homework);
			}
		}

		Pattern pattern = Pattern.compile("\\D*(?:(\\d*)\\/(\\d{1,2})\\/(\\d{2,4}))|(?:(\\d*)\\-(\\d{1,2})\\-(\\d{2,4}))|(?:((?:Dec(?:ember)*)|(?:Jan(?:uary)*)|(?:Feb(?:ruary)*)|(?:March)|(?:April)|(?:May)|(?:June)|(?:July)|(?:Aug(?:ust)*)|(?:Sept(?:ember)*)|(?:Sep(?:tember)*)|(?:Nov(?:ember)*))( \\d{1,2}))\\D*", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(lineWithDate);

		int theMonth = 0;
		int theDay = 0;
		int theYear = 0;

		if (matcher.matches()) {
			if (matcher.group(1) != null) {
				theMonth = Integer.valueOf(matcher.group(1));
				theDay = Integer.valueOf(matcher.group(2));
				theYear = Integer.valueOf(matcher.group(3));
			} else if (matcher.group(4) != null) {
				theMonth = Integer.valueOf(matcher.group(4));
				theDay = Integer.valueOf(matcher.group(5));
				theYear = Integer.valueOf(matcher.group(6));
			} else if (matcher.group(7) != null) {
				theMonth = Integer.valueOf(matcher.group(7));
				theDay = Integer.valueOf(matcher.group(8));
				theYear = Calendar.getInstance().get(Calendar.YEAR);
			} else {
				System.out.println("ERROR: Matcher group 1, 4, 7 have all returned 0. DueDate is incorrect");
			}

			if (theYear == 0 | theMonth == 0 | theDay == 0) {
				x.setDueDate(null);
			} else {
				x.setDueDate(new DateTime(theYear, theMonth, theDay, 23, 59, 0, 0));
			}
			if (x.getAssignmentType().equals(Assignment.AssignmentType.Test)) {
				x.setImportanceLevel(Assignment.ImportanceLevel.High);
			} else if (x.getAssignmentType().equals(Assignment.AssignmentType.Quiz)) {
				x.setImportanceLevel(Assignment.ImportanceLevel.Medium);
			} else if (x.getAssignmentType().equals(Assignment.AssignmentType.Homework)) {
				x.setImportanceLevel(Assignment.ImportanceLevel.Low);
			} else {
				x.setImportanceLevel(Assignment.ImportanceLevel.Unknown);
			}
			System.out.println("----End of scanDocument----\n");
			//Where we assign topic
		}
		asmt.add(x);
	}
}

// tgreen35@gsu.edu