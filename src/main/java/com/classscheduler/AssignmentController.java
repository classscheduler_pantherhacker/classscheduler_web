package com.classscheduler;

import com.classscheduler.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

/**
 Created by joshua on 10/1/15.
 */
@RequestMapping ("/assignment")
@RestController
@Transactional
public class AssignmentController {
	@Autowired private AssignmentRepository assignmentRepository;
	@Autowired private UserRepository       userRepository;
	@Autowired private CourseRepository     courseRepository;

	@RequestMapping ("/findAll")
	public Iterable<Assignment> findAll () {
		return assignmentRepository.findAll();
	}

	@RequestMapping ("/findAllForUser/{userId}")
	public List<Assignment> findAllForUser (@PathVariable (value = "userId") Long userId) {
		User user = userRepository.findOne(userId);
		//courseRepository.
		return assignmentRepository.findAllInCoursesOrderByDueDateAsc(user.getStudentCourses());
	}

	@RequestMapping ("/findAllForCourse/{courseId}")
	public List<Assignment> findAllForCourse (@PathVariable (value = "courseId") Long courseId) {
		return assignmentRepository.findAllByCourse(courseRepository.findOne(courseId));
	}

	@RequestMapping ("/findAllForCrn/{crn}")
	public List<Assignment> findAllForCourse (@PathVariable (value = "crn") int crn) {
		return assignmentRepository.findAllByCourse(courseRepository.findByCrn(crn));
	}
}


