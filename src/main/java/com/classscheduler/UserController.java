package com.classscheduler;

import com.classscheduler.domain.User;
import com.classscheduler.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

/**
 Created by joshua on 10/1/15.
 */
@RequestMapping ("/user")
@RestController
@Transactional
public class UserController {
	@Autowired private UserRepository userRepository;

	@RequestMapping ("/findAll")
	public Iterable<User> findAll () {
		return userRepository.findAll();
	}

	@RequestMapping ("/{id}")
	public User findById (@PathVariable ("id") Long id) {
		return userRepository.findOne(id);
	}

	@RequestMapping ("/findByLastName/{lastName}")
	public User findByLastName (@PathVariable (value = "lastName") String lastName) {
		return userRepository.findByLastName(lastName);
	}
}


