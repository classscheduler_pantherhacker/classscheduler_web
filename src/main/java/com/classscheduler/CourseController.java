package com.classscheduler;

import com.classscheduler.domain.Course;
import com.classscheduler.domain.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

/**
 Created by joshua on 10/1/15.
 */
@RequestMapping ("/course")
@RestController
@Transactional
public class CourseController {
	@Autowired private CourseRepository courseRepository;

	@RequestMapping ("/findAll")
	public Iterable<Course> findAll () {
		return courseRepository.findAll();
	}

	@RequestMapping ("/{courseId}")
	public Course getByCourseId (@PathVariable ("courseId") Long courseId) {
		return courseRepository.findOne(courseId);
	}

	@RequestMapping ("/findByCrn/{crn}")
	public Course getByCourseId (@PathVariable (value = "crn") int crn) {
		return courseRepository.findByCrn(crn);
	}
}