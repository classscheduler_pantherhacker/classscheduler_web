package com.classscheduler;

import com.classscheduler.domain.TopicsCovered;
import com.classscheduler.domain.TopicsCoveredRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

/**
 Created by joshua on 10/1/15.
 */
@RequestMapping ("/topicsCovered")
@RestController
@Transactional
public class TopicsCoveredController {
	@Autowired private TopicsCoveredRepository topicsCoveredRepository;

	@RequestMapping ("/findAll")
	public Iterable<TopicsCovered> findAll () {
		return topicsCoveredRepository.findAll();
	}
}


