package com.classscheduler;

import com.classscheduler.domain.*;
import com.classscheduler.syllabusparsing.DocAnalysis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;

@RestController
@Configuration
@EnableAutoConfiguration
@ComponentScan
@EnableJpaRepositories
@ImportResource ("/WEB-INF/applicationContext.xml")
public class Application extends SpringBootServletInitializer {

	@RequestMapping ("/")
	String home () {
		return "Home.!";
	}

	@RequestMapping ("/isRunning")
	boolean isRunning () {
		return true;
	}

	public static void main (String[] args) {
		System.out.println("RUNNINGish! :D");
		ConfigurableApplicationContext run = SpringApplication.run(Application.class);
		System.out.println("RUNNING! :D");
	}

	@RequestMapping ("/apiDocs")
	String apiDocs () {
		try {
			return new String(Files.readAllBytes(new File("/Users/joshua/PantherHackers/classscheduler_web/apiDocs.html").toPath()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "404...";
	}

	@RequestMapping ("/parsePdf")
	boolean parsePdf () {
		DocAnalysis docAnalysis = new DocAnalysis("/Users/joshua/PantherHackers/classscheduler_web/2510_schedule.pdf");
		boolean b = docAnalysis.evaluateDoc();
		if (b) {
			ArrayList<Assignment> assignments = docAnalysis.parseAssignments();
			Course byCrn = courseRepository.findByCrn(87317);
			for (Assignment assignment : assignments) {
				assignment.setCourse(byCrn);
				assignmentRepository.save(assignment);
			}
			byCrn.setAssignments(assignments);
			courseRepository.save(byCrn);
		}
		return b;
	}

	@Autowired private CourseRepository        courseRepository;
	@Autowired private UserRepository          userRepository;
	@Autowired private AssignmentRepository    assignmentRepository;
	@Autowired private TopicsCoveredRepository topicsCoveredRepository;
	static boolean hasNotResetDBYet = true;
}