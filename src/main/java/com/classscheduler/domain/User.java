package com.classscheduler.domain;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Collection;

/**
 Created by joshua on 10/28/15.
 */
@Entity
@Transactional
public class User {
	private long id;

	@Id
	@Column (name = "id", nullable = false, insertable = true, updatable = true)
	public long getId () {
		return id;
	}

	public void setId (long id) {
		this.id = id;
	}

	private String firstName;

	@Basic
	@Column (name = "firstName", nullable = false, insertable = true, updatable = true, length = 50)
	public String getFirstName () {
		return firstName;
	}

	public void setFirstName (String firstName) {
		this.firstName = firstName;
	}

	private String lastName;

	@Basic
	@Column (name = "lastName", nullable = false, insertable = true, updatable = true, length = 50)
	public String getLastName () {
		return lastName;
	}

	public void setLastName (String lastName) {
		this.lastName = lastName;
	}

	private String username;

	@Basic
	@Column (name = "username", nullable = false, insertable = true, updatable = true, length = 100)
	public String getUsername () {
		return username;
	}

	public void setUsername (String username) {
		this.username = username;
	}

	private String password;

	@Basic
	@Column (name = "password", nullable = true, insertable = true, updatable = true, length = 30)
	public String getPassword () {
		return password;
	}

	public void setPassword (String password) {
		this.password = password;
	}

	private String email;

	@Basic
	@Column (name = "email", nullable = true, insertable = true, updatable = true, length = 100)
	public String getEmail () {
		return email;
	}

	public void setEmail (String email) {
		this.email = email;
	}

	private Timestamp memberSince;

	@Basic
	@Column (name = "memberSince", nullable = false, insertable = true, updatable = true)
	public Timestamp getMemberSince () {
		return memberSince;
	}

	public void setMemberSince (Timestamp memberSince) {
		this.memberSince = memberSince;
	}

	@Override
	public int hashCode () {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (username != null ? username.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (memberSince != null ? memberSince.hashCode() : 0);
		return result;
	}

	@Override
	public boolean equals (Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		User user = (User) o;

		if (id != user.id) { return false; }
		if (email != null ? !email.equals(user.email) : user.email != null) { return false; }
		if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) { return false; }
		if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) { return false; }
		if (memberSince != null ? !memberSince.equals(user.memberSince) : user.memberSince != null) { return false; }
		if (password != null ? !password.equals(user.password) : user.password != null) { return false; }
		if (username != null ? !username.equals(user.username) : user.username != null) { return false; }

		return true;
	}

	private Collection<Course> studentCourses;

	@ManyToMany (mappedBy = "students")
	public Collection<Course> getStudentCourses () {
		return studentCourses;
	}

	public void setStudentCourses (Collection<Course> studentCourses) {
		this.studentCourses = studentCourses;
	}

	private Collection<Course> professorCourses;

	@OneToMany (mappedBy = "professor")
	public Collection<Course> getProfessorCourses () {
		return professorCourses;
	}

	public void setProfessorCourses (Collection<Course> professorCourses) {
		this.professorCourses = professorCourses;
	}
}
