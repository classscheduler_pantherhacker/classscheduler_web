package com.classscheduler.domain;

import org.joda.time.DateTime;

import javax.persistence.*;
import javax.transaction.Transactional;

/**
 Created by joshua on 10/28/15.
 */
@Entity
@Transactional
public class Assignment {
	private long id;

	public Assignment () {
		this.assignmentType = AssignmentType.Unknown;
		this.importanceLevel = ImportanceLevel.Unknown;
	}

	@Id
	@Column (name = "id", nullable = false, insertable = true, updatable = true)
	public long getId () {
		return id;
	}

	public void setId (long id) {
		this.id = id;
	}

	private AssignmentType assignmentType;

	@Basic
	@Column (name = "assignmentType", nullable = false, insertable = true, updatable = true, length = 20)
	public AssignmentType getAssignmentType () {
		return assignmentType;
	}

	public void setAssignmentType (AssignmentType assignmentType) {
		this.assignmentType = assignmentType;
	}

	private DateTime dueDate;

	@Basic
	@Column (name = "dueDate", nullable = true, insertable = true, updatable = true)
	public DateTime getDueDate () {
		return dueDate;
	}

	public void setDueDate (DateTime dueDate) {
		this.dueDate = dueDate;
	}

	private String studentNotes;

	@Basic
	@Column (name = "studentNotes", nullable = true, insertable = true, updatable = true, length = 2147483647)
	public String getStudentNotes () {
		return studentNotes;
	}

	public void setStudentNotes (String studentNotes) {
		this.studentNotes = studentNotes;
	}

	private String topic;

	@Basic
	@Column (name = "topic", nullable = true, insertable = true, updatable = true, length = 50)
	public String getTopic () {
		return topic;
	}

	public void setTopic (String topic) {
		this.topic = topic;
	}

	private ImportanceLevel importanceLevel;

	@Basic
	@Column (name = "importanceLevel", nullable = true, insertable = true, updatable = true, length = 20)
	public ImportanceLevel getImportanceLevel () {
		return importanceLevel;
	}

	public void setImportanceLevel (ImportanceLevel importanceLevel) {
		this.importanceLevel = importanceLevel;
	}

	@Override
	public int hashCode () {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (assignmentType != null ? assignmentType.hashCode() : 0);
		result = 31 * result + (dueDate != null ? dueDate.hashCode() : 0);
		result = 31 * result + (studentNotes != null ? studentNotes.hashCode() : 0);
		result = 31 * result + (topic != null ? topic.hashCode() : 0);
		result = 31 * result + (importanceLevel != null ? importanceLevel.hashCode() : 0);
		return result;
	}

	@Override
	public boolean equals (Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		Assignment that = (Assignment) o;

		if (id != that.id) { return false; }
		if (course != null ? !course.equals(that.course) : that.course != null) { return false; }
		if (assignmentType != null ? !assignmentType.equals(that.assignmentType) : that.assignmentType != null) { return false; }
		if (dueDate != null ? !dueDate.equals(that.dueDate) : that.dueDate != null) { return false; }
		if (importanceLevel != null ? !importanceLevel.equals(that.importanceLevel) : that.importanceLevel != null) { return false; }
		if (studentNotes != null ? !studentNotes.equals(that.studentNotes) : that.studentNotes != null) { return false; }
		if (topic != null ? !topic.equals(that.topic) : that.topic != null) { return false; }

		return true;
	}

	@Override
	public String toString () {
		return "Assignment{" +
			   "id=" + id +
			   ", assignmentType=" + assignmentType +
			   ", dueDate=" + dueDate +
			   ", studentNotes='" + studentNotes + '\'' +
			   ", topic='" + topic + '\'' +
			   ", importanceLevel=" + importanceLevel +
			   ", course=" + course +
			   '}';
	}

	private Course course;

	@ManyToOne
	@JoinColumn (name = "course_id", referencedColumnName = "id", nullable = false)
	public Course getCourse () {
		return course;
	}

	public void setCourse (Course course) {
		this.course = course;
	}

	public enum AssignmentType {
		Test, Homework, Quiz, ImportantDate, Project, Unknown
	}

	public enum ImportanceLevel {
		High, Medium, Low, Unknown
	}
}
