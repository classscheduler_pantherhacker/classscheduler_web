package com.classscheduler.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CourseRepository extends CrudRepository<Course, Long> {
	Course findByCrn (int crn);
	List<Course> findAllByProfessor (User professor);
}