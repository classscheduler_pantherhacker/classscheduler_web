package com.classscheduler.domain;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Collection;

/**
 Created by joshua on 10/28/15.
 */
@Entity
@Transactional
public class Course {
	private long id;

	@Id
	@Column (name = "id", nullable = false, insertable = true, updatable = true)
	public long getId () {
		return id;
	}

	public void setId (long id) {
		this.id = id;
	}

	private String name;

	@Basic
	@Column (name = "name", nullable = false, insertable = true, updatable = true, length = 50)
	public String getName () {
		return name;
	}

	public void setName (String name) {
		this.name = name;
	}

	private int crn;

	@Basic
	@Column (name = "crn", nullable = false, insertable = true, updatable = true)
	public int getCrn () {
		return crn;
	}

	public void setCrn (int crn) {
		this.crn = crn;
	}

	@Override
	public int hashCode () {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + crn;
		return result;
	}

	@Override
	public boolean equals (Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		Course course = (Course) o;

		if (crn != course.crn) { return false; }
		if (id != course.id) { return false; }
		if (name != null ? !name.equals(course.name) : course.name != null) { return false; }

		return true;
	}

	private Collection<Assignment> assignments;

	@OneToMany (mappedBy = "course")
	public Collection<Assignment> getAssignments () {
		return assignments;
	}

	public void setAssignments (Collection<Assignment> assignments) {
		this.assignments = assignments;
	}

	private Collection<User> students;

	@ManyToMany
	@JoinTable (name = "course_students", catalog = "class_scheduler", schema = "", joinColumns = @JoinColumn (name = "course_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn (name = "student_id", referencedColumnName = "id", nullable = false))
	public Collection<User> getStudents () {
		return students;
	}

	public void setStudents (Collection<User> students) {
		this.students = students;
	}

	private Collection<TopicsCovered> topicsCovered;

	@OneToMany (mappedBy = "course")
	public Collection<TopicsCovered> getTopicsCovered () {
		return topicsCovered;
	}

	public void setTopicsCovered (Collection<TopicsCovered> topicsCovered) {
		this.topicsCovered = topicsCovered;
	}

	private User professor;

	@ManyToOne
	public User getProfessor () {
		return professor;
	}

	public void setProfessor (User professor) {
		this.professor = professor;
	}
}
