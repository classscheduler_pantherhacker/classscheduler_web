package com.classscheduler.domain;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.sql.Timestamp;

/**
 Created by joshua on 10/28/15.
 */
@Entity
@Transactional
@Table (name = "topic_covered", schema = "", catalog = "class_scheduler")
public class TopicsCovered {
	private long id;

	@Id
	@Column (name = "id", nullable = false, insertable = true, updatable = true)
	public long getId () {
		return id;
	}

	public void setId (long id) {
		this.id = id;
	}

	private Timestamp date;

	@Basic
	@Column (name = "date", nullable = false, insertable = true, updatable = true)
	public Timestamp getDate () {
		return date;
	}

	public void setDate (Timestamp date) {
		this.date = date;
	}

	private String description;

	@Basic
	@Column (name = "description", nullable = false, insertable = true, updatable = true, length = 100)
	public String getDescription () {
		return description;
	}

	public void setDescription (String description) {
		this.description = description;
	}

	@Override
	public int hashCode () {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (date != null ? date.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		return result;
	}

	@Override
	public boolean equals (Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		TopicsCovered that = (TopicsCovered) o;

		if (id != that.id) { return false; }
		if (course != null ? !course.equals(that.course) : that.course != null) { return false; }
		if (date != null ? !date.equals(that.date) : that.date != null) { return false; }
		if (description != null ? !description.equals(that.description) : that.description != null) { return false; }

		return true;
	}

	private Course course;

	@ManyToOne
	@JoinColumn (name = "course_id", referencedColumnName = "id", nullable = false)
	public Course getCourse () {
		return course;
	}

	public void setCourse (Course course) {
		this.course = course;
	}
}
