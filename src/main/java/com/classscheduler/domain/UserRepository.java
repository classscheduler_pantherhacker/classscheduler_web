package com.classscheduler.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, Long> {
	List<User> findAllByLastNameLike (String lastName);
	List<User> findAllByLastName (String lastName);
	User findByLastName (String lastName);
}