package com.classscheduler.util;

import org.springframework.core.io.support.ResourcePropertySource;

import java.io.IOException;

@SuppressWarnings ("unchecked")
public class PropertyUtils {
	public static <T> T getPropValue (String property) {
		try {
			String propFileName = "env.properties";
			ResourcePropertySource x = new ResourcePropertySource(propFileName);
			return (T) x.getProperty(property);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}